/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    extend: {
      fontFamily: {
        title: ['Georgia'],
        subtitle: ['Impact'],
        coming: ['Courier New']
      },
      fontSize: {
        '7xl': '5rem',
        '8xl': '7rem'
      },
      letterSpacing: {
        tightest: '-0.075em',
        superwide: '0.20em'
      },
      colors: {
        title: {
          s: '#d79b5d',
          a: '#bc643f',
          n: '#d58851',
          t: '#504c3e',
          u: '#bc643f',
          aa: '#b15933',
          r: '#d9ad6a',
          i: '#bc643f',
          o: '#bc643f'
        }
      }
    }
  },
  variants: {},
  plugins: []
}
